const htmlPdf = require('html-pdf-chrome');

const QRCode = require('qrcode');

const fs = require('fs');

const globAll = require('glob-all');

const mySite = 'https://test.sirikitty.ru'; // https://justplatform.com'
const dirForPdf = 'pdf';

const options = {
  port: 9222, // port Chrome is listening on
  completionTrigger: new htmlPdf.CompletionTrigger.Timer(5000),
  printOptions: {
    displayHeaderFooter: false,

  }
};


// собираю все файлы html для обработки в массив
const files = globAll.sync([
  'src/**/*.njk'
]);


// выбираю те файлы, для которых нужно сделать pdf
// для них прописано в шапке toPDF: true

const filesForPdf = [];

files.forEach(file => {
  try {
    const data = fs.readFileSync(file, 'utf8');

    if (data.includes('toPDF: true') && data.includes('---')) {
      filesForPdf.push(file);
    }

  } catch (error) {
    console.error('error with read file:', file);
    console.error(error);
  }

});



// начинаем делать pdf
filesForPdf.forEach(file => {

  // ссылка на страницу
  const url = mySite + file.replace('src', '').replace('.njk', '') + '/';
  console.log('url:', url);

  // название для файла
  const splitNameForPdf = file.split('/');
  let nameForPdf = splitNameForPdf[splitNameForPdf.length - 1];
  nameForPdf = nameForPdf.replace('.njk', '');
  console.log('название файла', nameForPdf);

  // путь к файлу
  const pathForPdf = file.replace('src', '').replace(nameForPdf, '').replace('.njk', '');
  console.log('путь к файлу:', pathForPdf);

  // проверяем, есть ли папка для нашей pdf. если нет, то создаём
  console.log(`/${dirForPdf}${pathForPdf}`);
  if(!fs.existsSync(`${dirForPdf}${pathForPdf}`)) {
    fs.mkdirSync(`${dirForPdf}${pathForPdf}`, { recursive: true }, (error) => {
      if (error) throw err;
    })
  };

  // проверяем, есть ли папка для нашего qrcode. если нет, то создаём
  // if(!fs.existsSync(`${dirForPdf}${pathForPdf}${nameForPdf}`)) {
  //   fs.mkdirSync(`${dirForPdf}${pathForPdf}${nameForPdf}`, { recursive: true }, (error) => {
  //     if (error) throw err;
  //   })
  // };

  // console.log('начинаем делать кьюар коды');

  /// сделаем qr-code
  // QRCode.toFile( `${dirForPdf}${pathForPdf}${nameForPdf}/qrcode.png`, url, {
  //   color: {
  //     dark: '#000',
  //     light: '#fff'
  //   }
  // }, function (err) {
  //   if (err) throw err
  //   console.log(`${pathForPdf}${nameForPdf}.png готов`)
  // });

  // console.log('закончили делать кьюар коды');



  console.log('начали делать PDF');
  // и осталось сделать да положить туда pdf!
  htmlPdf.create(url, options)
    .then((pdf) => pdf.toFile(`${dirForPdf}${pathForPdf}/${nameForPdf}.pdf`));
  console.log('закончили делать pdf');
});
