const fs = require('fs');

const path = require('path')

const sharp = require('sharp');

const globAll = require('glob-all');

const imagesPathConfig = require('./imagesPathConfig.js');

const SITEDIR = imagesPathConfig.processedPhotosBaseDirectory;
const IMGDIR = imagesPathConfig.processedPhotosImagesDirectory;

const respSizes = [ 400, 600, 800, 1000, 1200, 1400, 1600, 2000 ];


// собираю все файлы для обработки в массив
const files = globAll.sync([
  `${imagesPathConfig.initialPhotosImagesDirectory}/**/*.{jpg,JPG,jpeg,JPEG}`
])


// собираю папки для файлов в массив (ну сначала в сет, а потом в массив 😉)
const setPathForImages = new Set();

files.forEach(image => {
  const r = /[^\/]*$/;
  // удаляем название файла
  let fullPath = image.replace(r, '');

  // удаляем путь к основной директории
  fullPath = fullPath.replace(imagesPathConfig.initialPhotosBaseDirectory, '');

  setPathForImages.add(fullPath);
});

// здесь у нас папки, которые нужно создать
const pathForImages = Array.from(setPathForImages);


// создаю директории
if(!fs.existsSync(SITEDIR)) {
  fs.mkdirSync(SITEDIR)
}
if(!fs.existsSync(IMGDIR)) {
  fs.mkdirSync(IMGDIR)
}

// создаю директории для фото
pathForImages.forEach(imagePath => {
  fs.mkdirSync(SITEDIR + '/' + imagePath, { recursive: true }, (error) => {
    if (error) throw err;
  });
});


// Ищем изображения, которые обрабатывать не нужно (уже готовые)

// все изображения, которые уже обработанны
const filesDone = globAll.sync(`${imagesPathConfig.processedPhotosImagesDirectory}/**/*-w400.jpeg`);


const newImages = []; // те фото, которые нужно обработать будут здесь

for (let i = 0; i < files.length; i++) {
  let tempIn = files[i].replace(/\.[^/.]+$/, ""); // убираем расширение
  tempIn = tempIn.replace(imagesPathConfig.initialPhotosBaseDirectory, ""); // обрезаем начало пути
  tempIn = imagesPathConfig.processedPhotosBaseDirectory + tempIn + '-w400.jpeg'; // подстраиваем под уже обработанный файл

  // если такого файла нет в архиве, то нужно обработать
  if (!filesDone.includes(tempIn)) {
    newImages.push(files[i])
  }
}

// и пошла жара с картинками 🔥
newImages.forEach((item) => {

  let filePath = item.replace(imagesPathConfig.initialPhotosBaseDirectory, '').split('/'); // удаляю основной путь из строки (src) и разбиваю на папки

  let fileName = filePath[filePath.length - 1]; // имя файла
  fileName = fileName.replace(/\.[^/.]+$/, ""); // убираем расширения файла из имени


  filePath.length = filePath.length - 1;
  filePath = filePath.join('/'); // путь к файлу


  /* начинаю обработку фото */
  respSizes.forEach(size => {

    sharp(item)
      .jpeg({
        quality: 80,
      })
      .resize({
        width: size,
        withoutEnlargement: false, // с увеличением
      })
      .toFile(`${SITEDIR}/${filePath}/${fileName}-w${size}.jpeg`)
      .then()
      .catch(error => {console.error(error + ' : ' + item)});


    // WebP options
    // level of CPU effort to reduce file size, integer 0-6 (optional, default 4)
    sharp(item)
      .webp({
        quality: 82,
        reductionEffort: 4
      })
      .resize({
        width: size,
        withoutEnlargement: false,
      })
      .toFile(`${SITEDIR}/${filePath}/${fileName}-w${size}.webp`)
      .then()
      .catch(error => {console.error(error, item)});

    // AVIF options
    // CPU effort vs file size, 0 (slowest/smallest) to 8 (fastest/largest) (optional, default 5)
    sharp(item)
      .avif({ quality: 64, speed: 4 })
      .resize({
        width: size,
        withoutEnlargement: false,
      })
      .toFile(`${SITEDIR}/${filePath}/${fileName}-w${size}.avif`)
      .then()
      .catch(error => {console.error(error, item)});

  });

});
