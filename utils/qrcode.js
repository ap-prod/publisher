const QRCode = require('qrcode');

const fs = require('fs');

const globAll = require('glob-all');

const mySite = 'https://test.sirikitty.ru'; // https://justplatform.com'
const dirForQR = 'qrcode';




// собираю все файлы html для обработки в массив
const files = globAll.sync([
  'src/**/*.njk'
]);


// выбираю те файлы, для которых нужно сделать pdf
// для них прописано в шапке toPDF: true

const filesForQR = [];

files.forEach(file => {
  try {
    const data = fs.readFileSync(file, 'utf8');

    if (data.includes('toPDF: true') && data.includes('---')) {
      filesForQR.push(file);
    }

  } catch (error) {
    console.error('error with read file:', file);
    console.error(error);
  }

});




filesForQR.forEach(file => {

  // ссылка на страницу
  const url = mySite + file.replace('src', '').replace('.njk', '') + '/';
  console.log('url:', url);

  // название для файла
  const splitNameForQR = file.split('/');
  let nameForQR = splitNameForQR[splitNameForQR.length - 1];
  nameForQR = nameForQR.replace('.njk', '');
  console.log('название файла', nameForQR);

  // путь к файлу
  const pathForQR = file.replace('src', '').replace(nameForQR, '').replace('.njk', '');
  console.log('путь к файлу:', pathForQR);



  // проверяем, есть ли папка для нашего qrcode. если нет, то создаём
  if(!fs.existsSync(`${dirForQR}${pathForQR}${nameForQR}`)) {
    fs.mkdirSync(`${dirForQR}${pathForQR}${nameForQR}`, { recursive: true }, (error) => {
      if (error) throw err;
    })
  };

  console.log('начинаем делать кьюар коды');

  /// сделаем qr-code
  QRCode.toFile( `${dirForQR}${pathForQR}${nameForQR}/qrcode.svg`, url, {
    color: {
      dark: '#000',
      light: '#fff'
    }
  }, function (err) {
    if (err) throw err
    console.log(`${pathForQR}${nameForQR}.png готов`)
  });

  console.log('закончили делать кьюар коды');





});
