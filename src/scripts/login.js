const userIcon = document.querySelector('.user__icon');
const userName = document.querySelector('.user__name');

if (userIcon) {
  userIcon.addEventListener('click', () => {
    userName.classList.toggle('user__name--visible')

  })
}

