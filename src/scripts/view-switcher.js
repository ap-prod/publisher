const viewSwitcherRadio = document.querySelectorAll('.view-switcher__radio');

const categoryBlock = document.querySelector('.category');

if (viewSwitcherRadio.length && categoryBlock) {

  function setupView() {
    // проверяем, есть ли в localStorage сохраненённый вид
    const savedView = getSavedView();

    console.log('savedView ==', savedView);

    // если вид есть, то выставляем в свитчере его
    if (savedView) {
      console.log('хм');
      const currentView = document.querySelector(`.view-switcher__radio[value=${savedView}]`);
      currentView.checked = true;
      switchView(savedView);

    }

    // вешаем слушатель событий на все инпуты
    [...viewSwitcherRadio].forEach((radio) => {
      radio.addEventListener('change', (event) => {

        // чистим localStorage для вида по умолчанию
        // или же сохраняем значение
        if (event.target.value === 'list') {
          console.log('надо запустить clear');
          clearView()
        } else {
          console.log('надо запустить saveView');
          saveView(event.target.value);
        }
        // переключаем вид
        switchView(event.target.value);
      })
    });

  }

  function getSavedView() {
    console.log('запускаем getSavedView');
    console.log('getSavedView =', localStorage.getItem('view'));
    return localStorage.getItem('view');
  }

  function saveView(view) {
    console.log('запускаем saveView');
    console.log('надо сохранить', view);
    localStorage.setItem('view', view);
  }

  function clearView() {
    console.log('запускаем clearView');
    localStorage.removeItem('view');
    categoryBlock.classList.remove('category--grid-large-view');
    categoryBlock.classList.remove('category--grid-small-view');
  }

  function switchView(view) {
    console.log('надо переключить вид на', view);

    if (view === 'grid-large') {
      categoryBlock.classList.remove('category--grid-small-view');
      categoryBlock.classList.add('category--grid-large-view');
    }

    if (view === 'grid-small') {

      categoryBlock.classList.remove('category--grid-large-view');
      categoryBlock.classList.add('category--grid-small-view');
    }
  }



  setupView();

}

