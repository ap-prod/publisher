const searchButton = document.querySelector('.search__button');

if (searchButton) {
  searchButton.addEventListener('click', (event) => {
    event.preventDefault();
    const searchInput = document.querySelector('.search__input');

    if (searchInput.value.trim().length > 2) {
      console.log('🔍 ');
      console.log('Нажали кнопку поиска');
      console.log('Пользователь хочет найти: ' + searchInput.value.trim());
      searchInput.value = '';
    } else {
      console.log('🛑')
      console.log('пользователь ввёл странный запрос, надо ему об этом сказать');
    }

  })
}


