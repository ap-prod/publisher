const footerSubmitButton = document.querySelector('.footer-form__submit-button');

if (footerSubmitButton) {
  footerSubmitButton.addEventListener('click', (event) => {
    event.preventDefault();

    sendMessage();

  })
}


async function sendMessage() {
  const firstValue = document.querySelector('.footer-form__first-label');

  if (firstValue.value) {
    // 🛑 останавливаем СПАМ
    return false;
  }

  const apiUrl = 'https://justplatform.com:8075/api/sendmessage/';
  const name = document.querySelector('.footer-form__username');

  const email = document.querySelector('.footer-form__email');

  const message = document.querySelector('.footer-form__message');

  const currentUrl = window.location.href;
  const currentLang = window.location.pathname.split('/')[1];
  const currentEmail = email.value.trim();
  const currentName = name.value.trim();
  const currentMessage = message.value.trim();

  const data = {
    email: currentEmail,
    name: currentName,
    comment: currentMessage,
    phone: 'no phone',
    country: currentLang,
    section: currentUrl
  }

  try {
    const response = await fetch(apiUrl, {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json'
      }
    });
    const json = await response.json();
    console.log('Успех:', JSON.stringify(json));
  } catch (error) {
    console.error('Ошибка:', error);
  }


  name.value = '';
  email.value = '';
  message.value = '';
}
