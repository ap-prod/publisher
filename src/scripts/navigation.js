const menuButton = document.querySelector('.menu__button');

const menuList = document.querySelector('.menu__main-menu');

const headerBlock = document.querySelector('.header-block');

const menuLevelOneHasChild = document.querySelectorAll('.menu-link__menu-icon-block--level-one');

const menuLevelTwo = document.querySelectorAll('.main-menu--level-two');

const menuLevelTwoHasChild = document.querySelectorAll('.menu-link__menu-icon-block--level-two');

const menuLevelThree = document.querySelectorAll('.main-menu--level-three');

if (menuButton) {
  menuButton.addEventListener('click', () => {
    let expanded = menuButton.getAttribute('aria-expanded') === 'true';

    menuButton.setAttribute('aria-expanded', !expanded);

    menuButton.classList.toggle('menu__button--open');

    menuList.classList.toggle('main-menu--open-level-one');


    headerBlock.classList.toggle('header-block--open-menu');


  });
}


if (menuLevelOneHasChild) {
  for(let i = 0; i < menuLevelOneHasChild.length; i++) {
    menuLevelOneHasChild[i].addEventListener('click', () => {

      menuLevelTwo[i].classList.toggle('main-menu--level-two-visible');

      menuLevelOneHasChild[i].classList.toggle('add-krest');
    });
  }
}



if (menuLevelTwoHasChild) {
  for(let i = 0; i < menuLevelTwoHasChild.length; i++) {
    menuLevelTwoHasChild[i].addEventListener('click', () => {

      menuLevelThree[i].classList.toggle('main-menu--level-three-visible');

      menuLevelTwoHasChild[i].classList.toggle('add-krest');
    });
  }
}



