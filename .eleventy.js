module.exports = function (eleventyConfig) {
  // отменяем игнорирование файлов из gitignore
  eleventyConfig.setUseGitIgnore(false);

  // то, что копируем без обработки
  eleventyConfig.addPassthroughCopy("src/fonts/");
  // eleventyConfig.addPassthroughCopy("src/images/lg-icons/");
  eleventyConfig.addPassthroughCopy("documents/");
  eleventyConfig.addPassthroughCopy("src/images/");
  eleventyConfig.addPassthroughCopy('./src/index.html')

  // navigation
  eleventyConfig.addPlugin(require('@11ty/eleventy-navigation'));


  // то, за чем следим, чтобы перезагрузить сервер
  eleventyConfig.addWatchTarget("./src/**/*.{scss,js}");


  // shortcode for pdf path
  eleventyConfig.addNunjucksShortcode('linkForPdf', function(path) {

    // смысл такой: сначала мы разбиваем строку с адресом в массив
    const pathSplitArray = path.split('/');

    // там дальше много лишнего получается, поэтому мы сначала вынимаем имя для будущего файла:
    const fileName = pathSplitArray[pathSplitArray.length - 2];

    // потом отрезаем лишнее с одного конца
    pathSplitArray.length = pathSplitArray.length - 2;

    // дальше соединяем всё это в одну строчку и получаем почти нужный нам путь
    let filePath = pathSplitArray.join('/');

    // а теперь точно нужный нам путь:
    const fullFilePath = `${filePath}/${fileName}.pdf`

    // скорее всего этот код можно улучшить, но это потом. когда-нибудь 🙂
    return fullFilePath;
  });


  // link for lang-swither
  eleventyConfig.addNunjucksShortcode("pageUrl", function(pageUrl) {
    return pageUrl.slice(4);
  });


  // Shortcode for image

  eleventyConfig.addNunjucksShortcode('image400', function (src, alt, className) {
    if (alt === undefined) {
      console.error('альты нужно заполнять!');
      alt = '';
    };

    alt = alt.replace('"', '');


    if (className === undefined) {
      className = '';
    }

    // убираем расширение файла
    let tempIndex;
    if (src[src.length - 4] === '.') {
      tempIndex = 4; // to cut .jpg
    } else {
      tempIndex = 5; // to cut .jpeg
    }
    src = src.slice(0, src.length - tempIndex);

    return `
    <picture>
      <source
        srcset="${src}-w400.avif 1x,
                ${src}-w600.avif 1.5x,
                ${src}-w800.avif 2x,
                ${src}-w1000.avif 2.5x,
                ${src}-w1200.avif 3x,
                ${src}-w1400.avif 3.5x,
                ${src}-w1600.avif 4x"
        type="image/avif">
      <source
        srcset="${src}-w400.webp 1x,
                ${src}-w600.webp 1.5x,
                ${src}-w800.webp 2x,
                ${src}-w1000.webp 2.5x,
                ${src}-w1200.webp 3x,
                ${src}-w1400.webp 3.5x,
                ${src}-w1600.webp 4x"
        type="image/webp">

      <img
        class="${className}"
        alt="${alt}"
        src="${src}-w400.jpeg"
        srcset="${src}-w400.jpeg 1x,
                ${src}-w600.jpeg 1.5x,
                ${src}-w800.jpeg 2x,
                ${src}-w1000.jpeg 2.5x,
                ${src}-w1200.jpeg 3x,
                ${src}-w1400.jpeg 3.5x,
                ${src}-w1600.jpeg 4x">
    </picture>
    `
  });




  eleventyConfig.addNunjucksShortcode('galleryLink', function (imagePath) {
    const newImagePath = imagePath.replace(/\.[^/.]+$/, "");
    return newImagePath;
  });


  return {

    passthroughFileCopy: true,
    markdownTemplateEngine: "njk",
    templateFormats: ['html', 'njk', 'md'],

    dir: {
      input: 'src',
      output: 'dist',
      include: '_includes'
    }
  }
}
