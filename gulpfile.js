const gulp = require('gulp');

const rm = require('gulp-rm');

const sass = require('gulp-sass')(require('sass'));

const sassGlob = require('gulp-sass-glob');

const autoprefixer = require('autoprefixer');

const gcmq = require('gulp-group-css-media-queries');

const cleanCSS = require('gulp-clean-css');

const sourcemaps = require('gulp-sourcemaps');

const concat = require('gulp-concat');

const babel = require('gulp-babel');

const uglify = require('gulp-uglify-es').default;

const gulpif = require('gulp-if');

const htmlmin = require('gulp-htmlmin');

const env = process.env.NODE_ENV;



const clean = () => {
  console.log('удаляю dist');
  return gulp.src('dist/**/*', { ignore: 'dist/photos/**/*.{jpeg,webp,avif}', read: false }).pipe(rm())
}

exports.clean = clean;



const cleanAll = () => {
  console.log('удаляю dist');
  return gulp.src('dist/**/*', { read: false }).pipe( rm() );
}

exports.cleanAll = cleanAll;



const copyImages = () => {
  console.log('копирую обработанные фотографии');
  return gulp.src('processed-images/**/*.{jpeg,webp,avif}').pipe(gulp.dest('dist'));
}

exports.copyImages = copyImages;


const styles = () => {
  console.log('преобразую scss в css')
  return gulp.src('src/styles/base.scss')
    .pipe(gulpif(env === 'dev', sourcemaps.init()))
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpif(env === 'prod', autoprefixer({
      cascade: false
    })))
    .pipe(gulpif(env === 'prod', gcmq()))
    .pipe(gulpif(env === 'prod', cleanCSS()))
    .pipe(gulpif(env === 'dev', sourcemaps.write()))
    .pipe(gulp.dest('dist/styles'));
}

exports.styles = styles;



const stylesLightGallery = () => {
  console.log('преобразую scss в css для lightgallery')
  return gulp.src('src/styles/lightgallery/lightgallery.scss')
    .pipe(gulpif(env === 'dev', sourcemaps.init()))
    .pipe(sassGlob())
    .pipe(sass().on('error', sass.logError))
    .pipe(gulpif(env === 'prod', autoprefixer({
      cascade: false
    })))
    .pipe(gulpif(env === 'prod', gcmq()))
    .pipe(gulpif(env === 'prod', cleanCSS()))
    .pipe(gulpif(env === 'dev', sourcemaps.write()))
    .pipe(gulp.dest('dist/styles'));
}

exports.stylesLightGallery = stylesLightGallery;


const js = () => {
  console.log('преобразую js в старый js');

  return gulp.src('src/scripts/*.js')
    .pipe(gulpif(env === 'dev', gulp.src('utils/myDom.js')))
    .pipe(gulpif(env === 'dev', sourcemaps.init()))
    .pipe(concat('main.js'))
    .pipe(gulpif(env === 'prod', babel({
      presets: ['@babel/env']
    })))
    .pipe(gulpif(env === 'prod', uglify()))
    .pipe(gulpif(env === 'dev', sourcemaps.write()))
    .pipe(gulp.dest('dist/js'))
}

exports.js = js;



const lightGalleryJS = () => {
  console.log('преобразую js для lightGallery');

  return gulp.src('src/scripts/lightgallery/*.js')
    .pipe(gulpif(env === 'dev', sourcemaps.init()))
    .pipe(concat('lightgallery.js'))
    .pipe(gulpif(env === 'prod', babel({
      presets: ['@babel/env']
    })))
    .pipe(gulpif(env === 'prod', uglify()))
    .pipe(gulpif(env === 'dev', sourcemaps.write()))
    .pipe(gulp.dest('dist/js'))
}

exports.lightGalleryJS = lightGalleryJS;


const minifyHtml = () => {
  console.log('минимизирую html');

  return gulp.src('dist/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('dist'));
}

exports.minifyHtml = minifyHtml;





const myGulpServe = () => {
  // стандартный SCSS
  gulp.watch('./src/**/*.scss', gulp.series(styles));

  // lightgallery SCSS
  gulp.watch('./src/scripts/lightgallery/**/*.scss', gulp.series(stylesLightGallery));

  // обычный JS
  gulp.watch('./src/scripts/*.js', gulp.series(js));

  // lightgallery JS
  gulp.watch('./src/lightgallery/**/*.js', gulp.series( lightGalleryJS ));
}
exports.myGulpServe = myGulpServe;


// Default

exports.default = gulp.series(clean, gulp.parallel(styles, stylesLightGallery, js, lightGalleryJS));
